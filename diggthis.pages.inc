<?php

/**
 * @file
 * Pages for the diggthis module.
 */

/**
 * Do the Digg API request and retrieve the XML feed
 * with the top Digg stories for a given domain.
 *
 * @return $html
 * An HTML string with markup for the Digg stories or
 * an empty string if an error occurred
 *
 * @todo replace hard coded urls
 */
function diggthis_top_stories() {
  $html = '';

  $domain = trim(variable_get('diggthis_stories_domain', ''));
  if (empty($domain)) {
    drupal_set_message(t('There is no domain entered in the diggthis settings.'));
    return $html;
  }

  $appkey = url($_GET['q'], array('absolute' => TRUE));
  $domain = drupal_urlencode($domain);
  //$options = array(
  //  'query' => array('count' => 10,
  //                   'appkey' => $appkey,
  //                   'domain' => $domain,
  //                   'type' => 'php',
  //                   'sort' => 'digg_count-desc'),
  //  'absolute' => TRUE
  //);
  // ugly but calling url() twice does not work due to urlencodeing
  $query_string = '?count=10&sort=digg_count-desc&type=xml&appkey=' . $appkey . '&domain=' . $domain;
  $xml = diggthis_request($query_string);
  if ($xml) {
    $data = simplexml_load_string($xml);
    foreach ($data->story as $story) {
      $html .= theme('diggthis_story', $story);
    }
  }
  else {
    drupal_set_message(t('An error occurred. Digg top stories could not be retrieved.'));
  }
  return $html;
}