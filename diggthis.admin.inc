<?php

/**
 * @file
 * Admin settings for the diggthis module.
 */

/**
 * Admin settings form for the diggthis module
 *
 * make topic array from diggthis_topics file
 * perl -p -e "s/^(.+)\n?/'\$1',/g" modules/diggthis/diggthis_topics.txt
 */
function diggthis_admin_settings() {
  $path = drupal_get_path('module', 'diggthis');
  drupal_add_js($path . '/js/diggthis.js');
  drupal_add_js($path . '/js/diggthis.admin.js');
  drupal_add_css($path . '/css/diggthis.admin.css');
  $form = array();
  $topics = array('arts_culture', 'autos', 'baseball', 'basketball', 'business_finance', 'celebrity', 'comedy', 'comics_animation', 'design', 'educational', 'environment', 'extreme_sports', 'food_drink', 'football', 'gadgets', 'gaming_news', 'general_sciences', 'golf', 'hardware', 'health', 'hockey', 'linux_unix', 'microsoft', 'mods', 'motorsport', 'movies', 'music', 'nintendo', 'odd_stuff', 'olympics', 'other_sports', 'pc_games', 'people', 'pets_animals', 'playable_web_games', 'playstation', 'political_opinion', 'politics', 'programming', 'security', 'soccer', 'software', 'space', 'tech_news', 'television', 'tennis', 'travel_places', 'world_news', 'xbox');
  $form['diggthis'] = array(
    '#type' => 'fieldset',
    '#title' => t('diggthis settings')
  );

  $form['diggthis']['diggthis_button_skin'] = array(
    '#type' => 'select',
    '#title' => t('Button skin'),
    '#default_value' => variable_get('diggthis_button_skin', 'Medium'),
    '#options' => array(
      'Medium' => t('medium'),
      'Large' => t('large'),
      'Compact' => t('compact'),
      'Icon' => t('icon')
    ),
    '#description' => t('The Button skin option controls the look at the button. If set to <em>standard</em> the button defaults to a standard digg button (much like the one you see on Digg itself). If specified as <em>compact</em>, then a smaller horizontal visual design is used that will fit better into a list of links.'),
    '#attributes' => array('class' => 'diggthis_update_preview')
  );

  $form['diggthis']['diggthis_type'] = array(
    '#type' => 'select',
    '#title' => t('Digg media type'),
    '#default_value' => variable_get('diggthis_type', ''),
    '#options' => diggthis_select_options(array('news', 'image', 'video'), t('Select type')),
    '#description' => t("Pre select a media type for your site's content. Leave unselected to not set a media type."),
    '#attributes' => array('class' => 'diggthis_update_preview')
  );

  $form['diggthis']['diggthis_topic'] = array(
    '#type' => 'select',
    '#title' => t('Digg topic'),
    '#default_value' => variable_get('diggthis_topic', ''),
    '#options' => diggthis_select_options($topics, t('Select topic')),
    '#description' => t("Pre select a topic for your site's content. Leave unselected to not set a topic."),
    '#attributes' => array('class' => 'diggthis_update_preview')
  );

  $form['diggthis']['diggthis_button_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#default_value' => variable_get('diggthis_button_weight', 10),
    '#options' => drupal_map_assoc(range(-20, 20)),
    '#description' => t('Specifies the position of the Diggthis button. A low weight, e.g. <strong>-20</strong> will display the button above the content and a high weight, e.g. <strong>20</strong> below the content.')
  );

  $form['diggthis']['diggthis_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('diggthis_node_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('activate the node types in where the digg button shall be displayed')
  );

  $form['diggthis_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('diggthis API settings')
  );

  $form['diggthis_api']['diggthis_stories_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Digg Stories Domain'),
    '#default_value' => variable_get('diggthis_stories_domain', ''),
    '#size' => 32,
    '#maxlength' => 128,
    '#description' => t('Enter the domain, e.g. <em>mydomain.com</em>, that will be used to show top Digg stories for.')
  );

  $period = drupal_map_assoc(array(21600, 43200, 86400), 'format_interval');
  $form['diggthis_api']['diggthis_cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Minimum cache lifetime'),
    '#default_value' => variable_get('diggthis_cache_lifetime', 43200),
    '#options' => $period,
    '#description' => t('The time the response from the Digg API is cached before being requested again.')
  );

  $button = diggthis_button_create();
  $form['diggthis']['preview'] = array(
    '#value' => '<div id="diggthis_button_preview">' . $button . '</div>',
  );
  return system_settings_form($form);
}

function diggthis_select_options($options, $unselected) {
  $select[''] = $unselected;
  foreach ($options as $option) {
    $select[$option] = str_replace('_', ' ', $option);
  }
  return $select;
}