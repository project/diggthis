(function(){
  jQuery.fn.diggthis = function(options) {
    settings = jQuery.extend({
      skin: 'Medium'
    }, options);
    var a = jQuery(document.createElement('a'));
    a.attr('class','DiggThisButton Digg' + settings.skin);
    settings.url && a.attr('href', settings.url);
    settings.rev && a.attr('rev', settings.rev);
    jQuery(this).html(a);
    if (!window.__DBW) {
      var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
      s.type = 'text/javascript';
      s.async = true;
      s.src = 'http://widgets.digg.com/buttons.js';
      s1.parentNode.insertBefore(s, s1);
    }
  }
})();